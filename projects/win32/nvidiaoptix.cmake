set(nvidiaoptix_libdir lib64)
set(nvidiaoptix_libdest lib)
set(nvidiaoptix_libsuffix .lib)
set(nvidiaoptix_bindir bin64)
set(nvidiaoptix_bindest bin)
set(nvidiaoptix_binsuffix .dll)

include(nvidiaoptix.common)
